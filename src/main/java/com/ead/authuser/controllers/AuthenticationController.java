package com.ead.authuser.controllers;

import com.ead.authuser.dtos.UserDto;
import com.ead.authuser.services.UserService;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Log4j2
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    UserService userService;

    @PostMapping("/signup")
    public ResponseEntity<Object> registerUser(@RequestBody
                                                   @Validated(UserDto.UserView.RegistrationPost.class)
                                                   @JsonView(UserDto.UserView.RegistrationPost.class) UserDto userDto) {
        ResponseEntity<Object> responseError = verifyUserRegister(userDto);
        if (Objects.nonNull(responseError)) {
            return responseError;
        }
        var userModel = UserDto.toModel(userDto);
        userService.save(userModel);
        log.info("[POST registerUser]: User saved successfully userId {}", userModel.getUserID());
        return ResponseEntity.status(HttpStatus.CREATED).body(userModel);
    }

    private ResponseEntity<Object> verifyUserRegister(UserDto userDto) {
        if (userService.existsByUserName(userDto.getUsername())) {
            log.warn("[POST registerUser]: Username {} Already Taken!", userDto.getUsername());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Error: Username is Already Taken!");
        }
        if (userService.existsByEmail(userDto.getEmail())) {
            log.warn("[POST registerUser]: Email {} Already Taken!", userDto.getUsername());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Error: Email is Already Taken!");
        }
        return null;
    }

}
